
def createOption(num, description):
    return "\r\n#b#L" + str(num) + "#" + description


def getNx(account):
    return account.getNxCredit()


def hasEnoughNx(account, amount):
    return getNx(account) >= amount


def spendNx(account, amount):
    if hasEnoughNx(account, amount):
        account.deductNXCredit(amount)
        return True
    return False


def buyCube(cube, account):
    cubeid = cube["id"]
    amount = cube["amount"]
    price = cube["price"]
    name = cube["name"]
    canHold = sm.canHold(cubeid, amount)

    if not canHold:
        sm.sendNext("You don't have enough space in your inventory.")
        return False
    if not spendNx(account, price):
        sm.sendNext("You don't have enough NX.")
        return False
    sm.giveItem(cubeid, amount)
    sm.sendNext("You bought " + str(amount) + " " +
                name + " for " + str(price) + " NX.")
    return True


options = ["I want to go somewhere",
           "I want to buy something", "Buy Cubes", "Sell All Items"]

options2 = ["Town Maps", "Monster Maps", "Boss Entrances"]

maps = [
    [300000000, 680000000, 230000000, 910001000, 260000000, 541000000, 610050000, 540000000,
     211060010, 863100000, 105300000, 310000000, 211000000, 101072000, 101000000, 101050000,
     130000000, 820000000, 223000000, 410000000, 141000000, 120040000, 209000000, 682000000,
     310070000, 401000000, 100000000, 271010000, 251000000, 744000000, 551000000, 103000000,
     224000000, 241000000, 240000000, 104000000, 220000000, 150000000, 261000000, 701220000,
     807000000, 701210000, 250000000, 800000000, 600000000, 120000000, 200000000, 800040000,
     400000000, 102000000, 914040000, 865000000, 801000000, 105000000, 866190000, 693000020,
     270000000, 860000000, 273000000, 701100000, 320000000],  # Town Maps

    [240070300, 800020110, 610040000, 270030000, 860000032, 211060000, 240040500, 551030100,
     271000300, 211061000, 211041100, 240010501, 270020000, 910170100, 910160100, 610030010,
     863000100, 910180100, 272000300, 682010200, 541000300, 241000200, 220050300, 102040200,
     240010700, 241000210, 241000220, 272010000, 910028600, 706041000, 706041005, 273050000,
     231040400, 401050000, 541020000, 502010010],  # Monster Maps

    # [[105100100, "Balrog"], [211042300, "Zakum"], [240050400, "Horntail"], [262030000, "Hilla"],
    # [105200000, "Root Abyss"], [211070000, "Von Leon"], [272020110, "Arkarium"], [401060000, "Easy Magnus"],
    # [401060000, "Normal/Hard Magnus"], [270050000, "Pink Bean"], [271030600, "Cygnus"], [350060300, "Lotus"],
    # [863010000, "Gollux"], [211041700, "Ranmaru"], [811000008, "Princess No"], [970000106, "Hekaton"],
    # [970072200, "Ursus"], [105300303, "Damien"], [610030010, "Crimsonwood Keep"], [450004000, "Lucid"],
    # [927030060, "Black Mage"]] # Boss Maps
    [[211042300, "Zakum"], [240050400, "Horntail"], [262030000, "Hilla"], [211070000, "Von Leon"],
     [272020110, "Arkarium"], [271040000, "Cygnus"]]
]

cubes = [
    {
        "id": 5062009,
        "name": "Red Cube",
        "amount": 10,
        "price": 500000,
    },
    {
        "id": 5062009,
        "name": "Red Cube",
        "amount": 100,
        "price": 5000000,
    },
    {
        "id": 5062010,
        "name": "Black Cube",
        "amount": 10,
        "price": 600000,
    },
    {
        "id": 5062010,
        "name": "Black Cube",
        "amount": 100,
        "price": 6000000,
    },
    {
        "id": 5062024,
        "name": "Violet Cube",
        "amount": 10,
        "price": 1000000,
    },
    {
        "id": 5062024,
        "name": "Violet Cube",
        "amount": 100,
        "price": 10000000,
    },
    {
        "id": 5062500,
        "name": "Bonus Cube",
        "amount": 10,
        "price": 600000,
    },
    {
        "id": 5062500,
        "name": "Bonus Cube",
        "amount": 100,
        "price": 6000000,
    },
    {
        "id": 5062501,
        "name": "Special Bonus Cube",
        "amount": 10,
        "price": 1000000,
    },
    {
        "id": 5062501,
        "name": "Special Bonus Cube",
        "amount": 100,
        "price": 10000000,
    },
    {
        "id": 5062503,
        "name": "White Bonus Cube",
        "amount": 10,
        "price": 1000000,
    },
    {
        "id": 5062503,
        "name": "White Bonus Cube",
        "amount": 100,
        "price": 10000000,
    }
]

list = "Hello #r#h0##k! How can I help you today?"
i = 0
while i < len(options):
    list += "\r\n#b#L" + str(i) + "#" + str(options[i])
    i += 1
i = 0
option = sm.sendNext(list)
if option == 0:  # I want to go somewhere (maps)
    list = "These are your options: "
    while i < len(options2):
        list += "\r\n#b#L" + str(i) + "#" + str(options2[i])
        i += 1
    i = 0
    ans1 = sm.sendNext(list)
    list = "These are your options: "
    if ans1 == 2:  # boss maps
        while i < len(maps[ans1]):
            list += "\r\n#L" + str(i) + "##b" + str(maps[ans1][i][1])
            i += 1
    else:  # town/monster maps
        while i < len(maps[ans1]):
            list += "\r\n#L" + str(i) + "##b#m" + str(maps[ans1][i]) + "#"
            i += 1
    ans2 = sm.sendNext(list)
    if ans1 == 2:  # boss maps
        sm.warp(maps[ans1][ans2][0], 1)
    else:
        sm.warp(maps[ans1][ans2], 1)

elif option == 1:  # Want to buy
    sm.openShop(9201060)
elif option == 2:  # Buy Cubes
    character = sm.getChr()
    account = character.getAccount()
    question = "Select cubes you want to buy: "
    for n in range(len(cubes)):
        cube = cubes[n]
        amount = cube["amount"]
        price = cube["price"]
        name = cube["name"]
        question += createOption(n, str(amount) + " " +
                                 name + " for " + str(price) + " NX")
    while True:
        chosen = sm.sendNext(question)
        buyCube(cubes[chosen], account)
elif option == 3:  # Sell All
    areyousure = sm.sendAskYesNo(
        "Are you sure you want to sell all Equip items?")
    if areyousure:
        character = sm.getChr()
        equipInv = character.getEquipInventory()
        eqItems = equipInv.getItems()
        total = 0
        itemsSold = 0
        for nitem in range(len(eqItems)):
            eqitem = eqItems[0]
            # quantity = eqitem.getQuantity()
            total += eqitem.getPrice()  # * quantity
            sm.consumeItem(eqitem.getItemId(), 1)  # quantity)
            itemsSold += 1
        # equipInv.setItems([])
        sm.giveMesos(total)
       # equipInv.sortItemsByIndex()
        sm.sendSayOkay("Sold " + str(itemsSold) +
                       " items and earned " + str(total) + " mesos.")

else:
    sm.sendSayOkay("This option currently is uncoded.")
